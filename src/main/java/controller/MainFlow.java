package controller;

import model.BaseCharacter;
import model.entities.mario.Mario;
import model.entities.wizard.Wizard;

import static view.MainFlow.scannIntValue;
import static view.Utilities.*;

public class MainFlow {

    public static void redirectToPage(int choice) {
        if (choice == 1) {
            Mario.startMarioFlow(new Mario());
        } else if (choice == 2) {
            Wizard.startWizardFlow(new Wizard());
        } else {
            view.MainFlow.printMessage(INCORRECTVALUE);
            redirectToPage(scannIntValue());
        }
    }

    public static void finishGameIfHeroIsDead(BaseCharacter character) {
        if (!isAlive(character)) {
            System.out.println("GAME OVER");
            System.exit(0);
        }

    }

    public static boolean isAlive(BaseCharacter character) {
        return character.getHp() > 0;
    }

}
