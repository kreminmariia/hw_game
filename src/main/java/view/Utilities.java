package view;

public class Utilities {
    static final String WELCOMING = "Hi my dear friend! " +
            "\nToday you are able to transform into one of the most popular hero in the fantastic world. " +
            "\nThere are two characters with different stories. " +
            "\nYou need to choose one of them and go through difficulties to reach end point. Be bold! LET'S START!" +
            "\n 1 - Mario" +
            "\n 2 - Wizard";
    static final String DOYOURCHOICE = "Prepare your boots and do a choice, dude!";
    public static final String INCORRECTVALUE = "Nope nope. Type correct value!";
}
