package view;


import java.util.Scanner;

import static view.Utilities.*;

public class MainFlow {

    public static void startGame() {
        printMessage(WELCOMING);
        printSymbolQueue(".");
        printMessage(DOYOURCHOICE);
        controller.MainFlow.redirectToPage(scannIntValue());
    }

    public static int scannIntValue() {
        return sc.nextInt();
    }

    public static void printMessage(String stringToPrint) {
        System.out.println(stringToPrint);
    }

    public static void pause(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void printSymbolQueue(String toType) {
        for (int i = 3; i > 0; i--) {
            printMessage(toType + " ");
            pause(1);
        }
    }

    private static Scanner sc = new Scanner(System.in);
}
