package model;

import model.entities.Attacks;
import model.entities.TakesDamage;
import java.util.Random;

public abstract class BaseCharacter implements Attacks, TakesDamage {
    protected int hp;
    private int damageBoundary;

    public BaseCharacter(int attackPower) {
        damageBoundary = attackPower;
        hp = 100;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void decreaseHealth(int damage) {
        hp -= damage;
        showHpAmount();
    }

    public void attack(TakesDamage takesDamage) {
        takesDamage.decreaseHealth(getAttackPower());
    }

    protected void showHpAmount() {
        System.out.println(this.getClass().getSimpleName() + "'s hp is: " + getHp());
    }

    private int getAttackPower() {
        return new Random().nextInt(this.damageBoundary);
    }

}
