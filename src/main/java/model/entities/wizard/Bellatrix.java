package model.entities.wizard;

import model.BaseCharacter;

class Bellatrix extends BaseCharacter {

    Bellatrix() {
        super(0);
    }

    void heal(Voldemort valodya) {
        int healPower = 5;
        int health = valodya.getHp() + healPower;
        valodya.setHp(health);
        System.out.println("Voldemort was healed " + health);
    }
}
