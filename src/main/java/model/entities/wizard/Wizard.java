package model.entities.wizard;

import model.BaseCharacter;
import model.entities.Attacks;
import model.entities.MainHero;
import model.entities.TakesDamage;
import view.MainFlow;

import static view.MainFlow.printSymbolQueue;
import static view.Utilities.INCORRECTVALUE;

public class Wizard extends BaseCharacter implements MainHero {
    private static final String GREETINGWIZARD = "You are in Hogwarts. We should save Dambldor, did you understand?";
    private static final String FIRSTENEMY = "\n\nHere are the door to Azkaban and so many dementors inside." +
            "Are you sure you wanna be here? \n\n1. Definitely no, run away from here! " +
            "\n\n2. Let's stay. They are funny";
    private static final String GAMEOVERDEMENTOR = "Cool. They stole you soul and you are dead. Is it fanny now?";
    private static final String SECONDENEMY = "\nOh God! There are Voldemort and Bellatrix. Hope we wont die. We have only one choice. Fight";
    private static final String ENDPOINT = "\n\n\n We won. And look! It is Dambldor. Dead. Damn!";

    public Wizard() {
        super(60);
    }

    public static void startWizardFlow(BaseCharacter character) {
        MainHero wizard = (MainHero) character;
        wizard.greeting();
        printSymbolQueue("\nWe are running");
        wizard.meetFirstEnemy();
        wizard.whatHeroDoWithEnemy(view.MainFlow.scannIntValue(), character);
        printSymbolQueue("\nWe are moving on");
        wizard.meetSecondEnemy();
        MainFlow.pause(3);
        ((MainHero) character).fightWith(new Voldemort(), character);
        wizard.reachEndPoint();
    }

    @Override
    public void whatHeroDoWithEnemy(int choice, BaseCharacter character) {
        if (choice == 1) {
        } else if (choice == 2) {
            escapeFromFight();
            System.exit(0);
        } else {
            System.out.println(INCORRECTVALUE);
            whatHeroDoWithEnemy(view.MainFlow.scannIntValue(), character);
        }
    }

    @Override
    public void fightWith(BaseCharacter enemy, BaseCharacter character) {
        Bellatrix bella = new Bellatrix();
        Attacks attacks = enemy;
        TakesDamage takesDamage = character;

        while (controller.MainFlow.isAlive(enemy) && controller.MainFlow.isAlive(character)) {
            attacks.attack(takesDamage);
            bella.heal((Voldemort) enemy);
            Object tem = (Object) takesDamage;
            takesDamage = (TakesDamage) attacks;
            attacks = (Attacks) tem;
        }
        controller.MainFlow.finishGameIfHeroIsDead(character);
    }

    @Override
    public void greeting() {
        view.MainFlow.printMessage(GREETINGWIZARD);
    }

    @Override
    public void meetFirstEnemy() {
        view.MainFlow.printMessage(FIRSTENEMY);
    }

    @Override
    public void escapeFromFight() {
        view.MainFlow.printMessage(GAMEOVERDEMENTOR);
    }

    @Override
    public void meetSecondEnemy() {
        view.MainFlow.printMessage(SECONDENEMY);
    }

    @Override
    public void reachEndPoint() {
        MainFlow.printMessage(ENDPOINT);
    }
}
