package model.entities.mario;

import controller.MainFlow;
import model.BaseCharacter;
import model.entities.Attacks;
import model.entities.MainHero;
import model.entities.TakesDamage;
import java.util.LinkedList;
import java.util.Queue;

import static view.MainFlow.printSymbolQueue;
import static view.Utilities.INCORRECTVALUE;

public class Mario extends BaseCharacter implements MainHero {
    private static final String GREETING = "IT'S-A ME, MARIO! Let's save my princess. ";
    private static final String FIRSTENEMY = "\n\nWOW! who is this? MUSHROOOOOM! What we gonna do?" +
            " \n\n1. Fight! \n2. Escape!";
    private static final String SECONDENEMY = "\n\nWOW! New Enemy...Flying Dragon! What we gonna do?" +
            " \n\n1. Fight! \n2. I will find another princess! ¯_(ツ)_/¯";
    private static final String YOUAREINTHECASTLE = "\n\nCongratulation! You are in the castle. " +
            "\n\n\n But your princess is not here. \nGame over, dude!";
    private static final String ESCAPEFROMGAME = "\n\nYou finish your game here. You lose but you are alive :)";

    public Mario() {
        super(50);
    }

    private static Queue<BaseCharacter> enemyQueue = new LinkedList<>();

    static {
        enemyQueue.add(new Mushroom());
        enemyQueue.add(new Dragon());
    }

    public static void startMarioFlow(BaseCharacter character) {
        MainHero mario = (MainHero) character;
        mario.greeting();
        printSymbolQueue("\nWe are running");
        mario.meetFirstEnemy();
        mario.whatHeroDoWithEnemy(view.MainFlow.scannIntValue(), character);
        printSymbolQueue("\nWe are moving on");
        mario.meetSecondEnemy();
        mario.whatHeroDoWithEnemy(view.MainFlow.scannIntValue(), character);
        mario.reachEndPoint();
    }

    @Override
    public void whatHeroDoWithEnemy(int choice, BaseCharacter character) {
        if (choice == 1) {
            fightWith(enemyQueue.poll(), character);
        } else if (choice == 2) {
            escapeFromFight();
            System.exit(0);
        } else {
            System.out.println(INCORRECTVALUE);
            whatHeroDoWithEnemy(view.MainFlow.scannIntValue(), character);
        }
    }

    @Override
    public void fightWith(BaseCharacter enemy, BaseCharacter character) {
        Attacks attacks = enemy;
        TakesDamage takesDamage = character;

        while (MainFlow.isAlive(enemy) && MainFlow.isAlive(character)) {
            attacks.attack(takesDamage);
            Object tem = (Object) takesDamage;
            takesDamage = (TakesDamage) attacks;
            attacks = (Attacks) tem;
        }
        MainFlow.finishGameIfHeroIsDead(character);
    }

    @Override
    public void greeting() {
        view.MainFlow.printMessage(GREETING);
    }

    @Override
    public void meetFirstEnemy() {
        view.MainFlow.printMessage(FIRSTENEMY);
    }

    @Override
    public void meetSecondEnemy() {
        view.MainFlow.printMessage(SECONDENEMY);
    }

    @Override
    public void reachEndPoint() {
        view.MainFlow.pause(3);
        System.out.println("\n\nWe need break door to castle");
        Obstacle door = new Obstacle();
        while (door.hp > 0) {
            door.decreaseHealth(30);
            System.out.println(getHp());
        }
        view.MainFlow.pause(4);
        view.MainFlow.printMessage(YOUAREINTHECASTLE);
    }

    @Override
    public void escapeFromFight() {
        view.MainFlow.printMessage(ESCAPEFROMGAME);
    }
}
