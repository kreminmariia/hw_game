package model.entities.mario;

import model.BaseCharacter;

public class Dragon extends BaseCharacter {

    Dragon() {
        super(25);
    }

    @Override
    public void decreaseHealth(int damage) {
        if (damage >= 40) {
            flyAway();
            System.out.println("\n\nIt was powerful shot! Dragon flied away!");
        } else {
            hp -= damage;
        }
        showHpAmount();
    }

    private void flyAway() {
        hp = 0;
    }
}
