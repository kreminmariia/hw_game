package model.entities.mario;

import model.entities.TakesDamage;

public class Obstacle implements TakesDamage {
    int hp = 30;

    public void decreaseHealth(int damage) {
        hp -= damage;
    }
}
