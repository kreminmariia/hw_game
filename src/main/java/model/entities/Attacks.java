package model.entities;

public interface Attacks {
    void attack(TakesDamage takesDamage);
}
