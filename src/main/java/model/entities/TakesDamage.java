package model.entities;

public interface TakesDamage {
    void decreaseHealth(int damage);
}
