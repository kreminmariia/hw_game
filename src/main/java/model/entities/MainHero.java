package model.entities;

import model.BaseCharacter;

public interface MainHero {
    void whatHeroDoWithEnemy(int choice, BaseCharacter character);

    void fightWith(BaseCharacter enemy, BaseCharacter character);

    void greeting();

    void meetFirstEnemy();

    void escapeFromFight();

    void meetSecondEnemy();

    void reachEndPoint();
}
